﻿using System;
using MVC_Example.Controllers;
using MVC_Example.MVCFramework;

namespace MVC_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            //Controller registration
            var book = new BookController();
            var author = new AuthorController();

            // Routing
            var router = new Router(new object[] {book, author});
            var (success, message) = router.Route(args);
            if (!success)
            {
                Console.WriteLine(message);
            }
        }
    }
}