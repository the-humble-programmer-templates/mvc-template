using System;

namespace MVC_Example.Models
{
    public class BookModel
    {
        public string Title;
        public int pages;

        public Guid Id;
        public Guid Author;

        public BookModel()
        {
        }

        public BookModel(string title, int pages, Guid id, Guid author)
        {
            Title = title;
            this.pages = pages;
            Id = id;
            Author = author;
        }
    }
}