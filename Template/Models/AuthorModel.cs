using System;
using System.IO;
using MVC_Example.MVCFramework;

namespace MVC_Example.Models
{
    public class AuthorModel
    {
        public string Name;
        public int Age;

        public Guid Id;
        public Guid[] books;

        public AuthorModel()
        {
        }

        public AuthorModel(string name, int age, Guid id)
        {
            Name = name;
            Age = age;
            Id = id;
            this.books = new Guid[] { };
        }
    }
}