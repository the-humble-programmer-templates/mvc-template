using System;
using System.Linq;
using Kirinnee.Helper;
using MVC_Example.Models;
using MVC_Example.MVCFramework;
using MVC_Example.View;

namespace MVC_Example.Controllers
{
    [Controller("book")]
    public class BookController
    {
        private Database _db = new Database();
        private BookView _view = new BookView();

        [Endpoint("create {title} {pages} {author}")]
        public void CreateBook(string title, string pages, string author = null)
        {
            try
            {
                //Create book model
                var id = Guid.NewGuid();
                var book = new BookModel(title, int.Parse(pages), id, author == null ? Guid.Empty : new Guid(author));

                //Save it
                _db.Save("book", id.ToString(), book);

                //Update the view
                _view.CreatedBook(book);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                _view.CreatedBook(null);
            }
        }

        [Endpoint("delete {id}")]
        public void DeleteBook(string id)
        {
            // Load book
            var book = _db.Load<BookModel>("book", id);
            if (book != null)
            {
                _db.Delete("book", id);
            }

            //Render the bview
            _view.Delete(book);
        }

        [Endpoint("get {id}")]
        public void Get(string id)
        {
            // Load book
            var book = _db.Load<BookModel>("book", id);

            // Check if author exists
            var authId = book.Author.ToString();
            var author = _db.Load<AuthorModel>("author", authId);

            _view.Details(book, author);
        }

        [Endpoint("update {id} title {title}")]
        public void UpdateTitle(string id, string title)
        {
            // Load book
            var book = _db.Load<BookModel>("book", id);
            if (book != null)
            {
                book.Title = title;
                _db.Save("book", book.Id.ToString(), book);
            }

            _view.UpdateTitle(book);
        }

        [Endpoint("list {limit} {contains}")]
        public void ListBooks(string limit = null, string contains = null)
        {
            var books = _db.TableKeys("book").Select(x => _db.Load<BookModel>("book", x));
            if (limit != null)
            {
                books = books.Take(int.Parse(limit));
            }

            if (contains != null)
            {
                books = books.Where(x => x.Title.Contains(contains));
            }
            _view.Listbooks(books.ToArray());
        }
    }
}