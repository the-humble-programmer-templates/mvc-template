using System;
using System.Linq;
using MVC_Example.Models;
using MVC_Example.MVCFramework;
using MVC_Example.View;

namespace MVC_Example.Controllers
{
    [Controller("author")]
    public class AuthorController
    {
        private Database _db = new Database();
        private AuthorView _view = new AuthorView();

        [Endpoint("create {name} {age}")]
        public void Create(string name, string age)
        {
            try
            {
                //Create author model
                var id = Guid.NewGuid();
                var author = new AuthorModel(name, int.Parse(age), id);

                //Save it
                _db.Save("author", id.ToString(), author);

                //Update the view
                _view.CreateAuthor(author);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                _view.CreateAuthor(null);
            }
        }

        [Endpoint("delete {id}")]
        public void Delete(string id)
        {
            // Load author
            var author = _db.Load<AuthorModel>("author", id);
            if (author != null)
            {
                _db.Delete("author", id);
            }

            //Render the bview
            _view.Delete(author);
        }

        [Endpoint("get {id}")]
        public void Get(string id)
        {
            // Load author
            var author = _db.Load<AuthorModel>("author", id);

            // Check if author exists
            var books = author.books.Select(x => _db.Load<BookModel>("book", x.ToString())).ToArray();
            _view.Details(author, books);
        }

        [Endpoint("update {id} age {age}")]
        public void UpdateAge(string id, string age)
        {
            // Load author
            var author = _db.Load<AuthorModel>("author", id);
            if (author != null)
            {
                author.Age = int.Parse(age);
                _db.Save("author", author.Id.ToString(), author);
            }

            _view.UpdateAge(author);
        }

        [Endpoint("list {limit} {contains}")]
        public void List(string limit = null, string contains = null)
        {
            var author = _db.TableKeys("author").Select(x => _db.Load<AuthorModel>("author", x));
            if (limit != null)
            {
                author = author.Take(int.Parse(limit));
            }

            if (contains != null)
            {
                author = author.Where(x => x.Name.Contains(contains));
            }

            _view.ListAuthor(author.ToArray());
        }

        [Endpoint("{author} add {book}")]
        public void AddBook(string author, string book)
        {
            var bookModel = _db.Load<BookModel>("book", book);
            var authorModel = _db.Load<AuthorModel>("author", author);
            if (bookModel != null && authorModel != null)
            {
                bookModel.Author = authorModel.Id;
                authorModel.books = authorModel.books.Append(bookModel.Id).ToArray();
                _db.Save("book", bookModel.Id.ToString(), bookModel);
                _db.Save("author", authorModel.Id.ToString(), authorModel);
            }

            _view.AddBooks(authorModel, bookModel);
        }

        [Endpoint("{author} remove {book}")]
        public void RemoveBook(string author, string book)
        {
            var bookModel = _db.Load<BookModel>("book", book);
            var authorModel = _db.Load<AuthorModel>("author", author);
            if (bookModel != null && authorModel != null)
            {
                bookModel.Author = Guid.Empty;
                authorModel.books = authorModel.books.Where(x => x != bookModel.Id).ToArray();
                _db.Save("book", bookModel.Id.ToString(), bookModel);
                _db.Save("author", authorModel.Id.ToString(), authorModel);
            }

            _view.Remove(authorModel, bookModel);
        }
    }
}