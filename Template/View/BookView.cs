using System;
using System.Collections.Generic;
using MVC_Example.Models;

namespace MVC_Example.View
{
    public class BookView
    {
        public void CreatedBook(BookModel bookModel)
        {
            if (bookModel == null)
            {
                Console.WriteLine("Failed to create book");
                return;
            }

            var x = $"Book <{bookModel.Title}> is created with ID {bookModel.Id} ";
            Console.WriteLine(x);
        }

        public void UpdateTitle(BookModel bookModel)
        {
            if (bookModel == null)
            {
                Console.WriteLine("There is no such book");
                return;
            }

            var x = $"Book with ID {bookModel.Id}'s title has updated to {bookModel.Title}";
            Console.WriteLine(x);
        }

        public void Details(BookModel bookModel, AuthorModel author)
        {
            if (bookModel == null)
            {
                Console.WriteLine("There is no such book");
                return;
            }

            var details = $"Book: \n \tTitle: {bookModel.Title}\n\tID: {bookModel.Id}";
            if (author != null)
            {
                details += $"\n\tAuthor: {author.Name}";
            }

            details += $"\n\tPage: {bookModel.pages}";

            Console.WriteLine(details);
        }

        public void Delete(BookModel bookModel)
        {
            if (bookModel == null)
            {
                Console.WriteLine("That book does not exist!");
            }
            else
            {
                var x = $"Book <{bookModel.Title}> of ID {bookModel.Title} has been deleted";
                Console.WriteLine(x);
            }
        }

        public void Listbooks(BookModel[] bookModels)
        {
            if (bookModels == null)
            {
                Console.WriteLine("Bad Request");
                return;
            }

            foreach (var book in bookModels)
            {
                Console.WriteLine($"{book.Id}: {book.Title}");
            }
        }
    }
}