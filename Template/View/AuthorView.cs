using System;
using System.Collections.Generic;
using System.Linq;
using MVC_Example.Models;

namespace MVC_Example.View
{
    public class AuthorView
    {
        public void CreateAuthor(AuthorModel author)
        {
            if (author == null)
            {
                Console.WriteLine("Failed to create author");
                return;
            }

            var x = $"Author <{author.Name}> is created with ID {author.Id} ";
            Console.WriteLine(x);
        }

        public void UpdateAge(AuthorModel author)
        {
            if (author == null)
            {
                Console.WriteLine("There is no such author");
                return;
            }

            var x = $"Author with ID {author.Id}'s age has became {author.Age}";
            Console.WriteLine(x);
        }

        public void Details(AuthorModel author, BookModel[] models)
        {
            if (author == null)
            {
                Console.WriteLine("There is no such author");
                return;
            }

            var details = $"Author: \n \tName: {author.Name}\n\tID: {author.Id}";
            details += $"\n\tAge: {author.Age}";

            if (models != null && models.Length > 0)
            {
                details += "\n\tBooks:";
                details = models.Aggregate(details, (current, book) => current + $"\n\t\t- {book.Title}");
            }

            Console.WriteLine(details);
        }

        public void Delete(AuthorModel author)
        {
            if (author == null)
            {
                Console.WriteLine("That author does not exist!");
            }
            else
            {
                var x = $"Author <{author.Name}> of ID {author.Id} has been deleted";
                Console.WriteLine(x);
            }
        }

        public void ListAuthor(AuthorModel[] authorModels)
        {
            if (authorModels == null)
            {
                Console.WriteLine("Bad Request");
                return;
            }

            foreach (var auth in authorModels)
            {
                Console.WriteLine($"{auth.Id}: {auth.Name}");
            }
        }

        public void AddBooks(AuthorModel author, BookModel book)
        {
            if (author == null)
            {
                Console.WriteLine("Author does not exist");
                return;
            }

            if (book == null)
            {
                Console.WriteLine("Book does not exist");
                return;
            }

            Console.WriteLine($"{author.Name} has written a new book: {book.Title}!!");
        }

        public void Remove(AuthorModel author, BookModel book)
        {
            if (author == null)
            {
                Console.WriteLine("Author does not exist");
                return;
            }

            if (book == null)
            {
                Console.WriteLine("Book does not exist");
                return;
            }

            Console.WriteLine($"{author.Name} no longer own the book: {book.Title}!!");
        }
    }
}